"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EventNotifier_1 = require("@archjs/event-notifier/EventNotifier");
class DataBinder {
    constructor(data) {
        this._notifier = new EventNotifier_1.EventNotifier(['update']);
        this._dirty = {};
        this._updateObservers = {};
        if (data) {
            if (DataBinder.isBinder(data)) {
                data = data._data;
            }
            else if (typeof data !== 'object') {
                data = { value: data };
            }
            if (Array.isArray(data)) {
                this._data = [];
            }
            else {
                this._data = {};
            }
            for (var key in data) {
                if (typeof data[key] !== 'function') {
                    this.defineProperty(key, data[key]);
                }
                // ignore functions !
            }
        }
        else {
            this._data = {};
        }
    }
    bind(key, handler) {
        return this._notifier.on(key, handler);
    }
    defineProperty(key, value) {
        this._data[key] = value;
        this._notifier.createEvent(key);
        if (DataBinder.isBinder(this._data[key])) {
            let that = this;
            this._updateObservers[key] =
                this._notifier.on('update', function (data) {
                    that._data[key]['update'](data);
                });
        }
        Object.defineProperty(this, key, {
            get: function () {
                return this._data[key];
            },
            set: function (value) {
                this._dirty[key] = {};
                this._dirty[key].oldValue = this._data[key];
                this._data[key] = value;
                this._dirty[key].newValue = value;
                if (DataBinder.isBinder(this._dirty[key].oldValue)) {
                    this._updateObservers[key].unsubscribe();
                }
                if (DataBinder.isBinder(this._data[key])) {
                    if (DataBinder.isBinder(this._data[key])) {
                        let that = this;
                        this._updateObservers[key] =
                            this._notifier.on('update', function (data) {
                                that._data[key]['update'](data);
                            });
                    }
                }
                return this._data[key];
            },
            enumerable: true,
            configurable: true
        });
    }
    update() {
        this._notifier.trigger('update', this._dirty);
        for (var key in this._dirty) {
            this._notifier.trigger(key, this._dirty[key]);
        }
        delete this._dirty;
        this._dirty = {};
    }
    static isBinder(data) {
        return typeof data === 'object' && typeof data.bind === 'function';
    }
    static createRecursiveBinder(data) {
        var ret = new DataBinder({});
        for (var i in data) {
            var tmp = data[i];
            if (typeof tmp === 'object') {
                tmp = DataBinder.createRecursiveBinder(tmp);
            }
            if (typeof tmp !== 'function') {
                ret.defineProperty(i, tmp);
            }
        }
        return ret;
    }
}
exports.DataBinder = DataBinder;
//# sourceMappingURL=databinder.js.map