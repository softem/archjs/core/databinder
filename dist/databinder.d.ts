import { EventNotifier } from '@archjs/event-notifier/EventNotifier';
interface IStringMap<vType> {
    [key: string]: any;
}
interface IDirtyData extends IStringMap<any> {
    oldValue?: any;
    newValue?: any;
}
interface IDirtyDataMap extends IStringMap<IDirtyData> {
}
declare class DataBinder {
    protected _data: any;
    protected _notifier: EventNotifier;
    protected _dirty: IDirtyDataMap;
    protected _updateObservers: {};
    constructor(data?: any);
    bind(key: any, handler: any): any;
    defineProperty(key: any, value: any): void;
    update(): void;
    static isBinder(data: any): boolean;
    static createRecursiveBinder(data: any): DataBinder;
}
export { IStringMap, IDirtyData, IDirtyDataMap, DataBinder };
