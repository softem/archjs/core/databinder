module.exports = function(config) {
    config.set({
        karmaTypescriptConfig:{
            tsconfig: "./karma-tsconfig.json"
        },
        frameworks: ["jasmine", "karma-typescript"],
        files: [
            { pattern: "src/**/*.ts" },
            { pattern: "test/**/*.ts" }
        ],
        preprocessors: {
            "src/**/*.ts":  ["karma-typescript", "coverage"],
            "test/**/*.ts": ["karma-typescript"]
        },
        reporters: ["progress", "coverage", "jasmine-spec-runner" ,"karma-typescript"],
        coverageReporter: {
            dir : 'report/coverage/'
        },
        jasmineSpecRunnerReporter: {
            jasmineCoreDir: 'jasmine-core'
        },
        browsers: ["Chrome"]
    });
};