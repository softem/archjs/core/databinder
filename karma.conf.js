module.exports = function(config) {
    config.set({
        karmaTypescriptConfig:{
            tsconfig: "./karma-tsconfig.json"
        },
        frameworks: ["jasmine", "karma-typescript"],
        files: [
            { pattern: "src/**/*.ts" },
            { pattern: "test/**/*.ts" }
        ],
        preprocessors: {
            "src/**/*.ts": ["karma-typescript", "coverage"],
            "test/**/*.ts": ["karma-typescript"]
        },
        reporters: ["progress","spec-as-html", "coverage", "junit", "karma-typescript"],
        coverageReporter: {
            dir : 'report/coverage/'
        },
        specAsHtmlReporter : {
            dir :       "report",
            outputFile: "spec.html"
        },
        junitReporter: {
            outputDir:      "report",
            outputFile:     "junit.xml",
            useBrowserName: false
        },
        browsers: ["Chrome"]
    });
};