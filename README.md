# DataBinder 

## Info 

**language &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:** TypeScript

**module type &nbsp;:** 'commonJs'

**dependencies:** ["@archjs/event-notifier"](https://gitlab.com/softem/archjs/core/event-notifier)

**description&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:**

A simple Typescript data-bind library that permits external predicates to intercept changes in object properties. This library uses dirty pattern and gives the user the ability to trigger changes by calling the member method 'update'. 


## Test coverage
## Installation
`npm install @archjs/databinder`

package.json 

```javascript 
{ ...
   dependencies: {
     ...
     "@archjs/databinder": "^"
     ...
}
```

This plugin has an install script, so when you load this module via npm it will automatically erase it's unecesary files.
It uses "fs-extra" , "rimraf", "rmdir" @install.

For dev enviorment use 
`git clone git@gitlab.com:softem/archjs/core/databinder.git`
    
## Usage

### creation

a
```typescript
 import { DataBinder } from "@archjs/databinder/databinder";
 ...
 /* no input data */
 var model0 = new DataBinder();
 /* basic data type as input */
 var model1 = new DataBinder( 24 );
 /* array as input */
 var model2 = new DataBinder( [24] );
 /* json object as input */
 var model3 = new DataBinder( {age:24} );
```

#### DataBinder data from nested data
When we have a nested json data object and we want each child node of the tree structure to be bindable we need to use the static method `DataBinder.createRecursiveBinder( data )`

```typescript
 import { DataBinder } from "@archjs/databinder/databinder";
 var data  = { 
              user:{ 
                      id:24
                    , email:"test@test.com" 
              }
              , userInfo:{ 
                      age:24
                    , firstName: "John"
                    , lastName:  "Doe"
              } 
 };
 var model = DataBinder.createRecursiveBinder( data );
```
## Bind handler to property change
```typescript 
   dataBinder.bind( <key:string> , <handler:Function> );
```

**bind** -> method

**key**  -> string

**handler**-> 
```typescript 
function( data ){ ... } 
```
data -> 
```typescript 
{ oldValue:any, newValue:any } 
```

### example
```typescript
var position:DataBinder = new DataBinder( {x:10,y:20} );
position.bind('x',function(data){ 
      var dx = (data.oldValue-data.newValue);
      console.log('moved element with dx = '+dx+'px' ); 
});
position['x'] = 21;   // dirty flag activated for 'x' key.
                   // observers are not notified!

position['x'] = 31    // dirty flag activated for 'x' key

position.update(); // observers are notified
```
output: `moved element with dx = 21px`

### using library for html binding 
```typescript
import { DataBinder } from "@archjs/databinder/databinder";


var userListData = [];
for( var i =0; i< 10000; i++ ){
    userListData[i] = {
         id:  i
        ,username: 'test@test'+i+'.com'
        ,info: {
              firstName: 'John'+i
            , lastName: 'Doe'+i
            , age: 10+i
        }
    };
}

var model = DataBinder.createRecursiveBinder( userListData );
for( let key in model['_data'] ){
    var div     = document.createElement("div");
    var content = document.createTextNode( "[" + model[key]['id'] +  "] -> Hi" + model[key]['username'] );
    div.appendChild( content );
    let element = document.querySelector('#app').insertBefore(div,null);
    model[key].bind('username',function (data) {
        element.innerText = "[" + model[key]['id'] +  "] -> Hi" + model[key]['username'];
    });
    model.bind( key, function () {
        element.remove();
    });
}


window["model"] = model;

```
in console

```javascript
for( var i =0; i< 10000; i++ ){ 
    if(i%2){ 
        model[i].username = ' gg George ' 
    } 
}
model.update();
```

it will update all elements @even id and display something simmilar to :

```html
<div id="app">
    <div>[0] ->> Hitest@test0.com</div>
    <div>[1] -> Hi gg George     </div>
    <div>[2] -> Hitest@test2.com </div>
    <div>[3] -> Hi gg George     </div>
    <div>[4] -> Hitest@test4.com </div>
    <div>[5] -> Hi gg George     </div>
    <div>[6] -> Hitest@test6.com </div>
    <div>[7] -> Hi gg George     </div>
    <div>[8] -> Hitest@test8.com </div>
    <div>[9] -> Hi gg George     </div>
    <div>[10]-> Hitest@test10.com</div>
    ...
</div>
```