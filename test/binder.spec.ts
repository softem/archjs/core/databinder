import {DataBinder} from "../src/databinder";

describe('DataBinder', function () {
    describe('input data none', function () {
        it('instantiating', function () {
            expect( new DataBinder() ).toBeDefined();
        });
    });

    describe('input basic data type', function () {
        var model;
        it('instantiating', function () {
            expect( model = new DataBinder(24) ).toBeDefined();
        });
        describe('get', function () {
            it('model["value"]', function () {
                expect( model["value"] ).toEqual(24);
            });
        });
        describe('set', function () {
            it('model["value"] = < value >', function () {
                model["value"] = 13;
                expect(  model["value"] ).toEqual(13);
            });
            it('intercept using model.bind( "value" , < handler > )', function () {
                var probe = 0;
                model.bind('value', function (data) {
                    probe += data.newValue;
                });
                model["value"] = 24;
                model.update();
                expect(  probe ).toEqual(24);
            });
        });
    });

    describe('input object', function () {
        var model;
        it('instantiating', function () {
            expect( model = new DataBinder({age:24}) ).toBeDefined();
        });
        describe('get', function () {
            it('model[ < key > ]',function () {
                expect(model['age']).toEqual(24);
            });
        });
        describe('set', function () {
            it('model[ < key > ] = < value >', function () {
                model["age"] = 13;
                expect(  model["age"] ).toEqual(13);
            });
            it('intercept using model.bind( < key >, < handler > )', function () {
                var probe = 0;
                model.bind('age', function (data) {
                    probe += data.newValue;
                });
                model["age"] = 24;
                model.update();
                expect(  probe ).toEqual(24);
            });
        });
    });

    describe('input type DataBinder', function () {
        var source = new DataBinder( {a:24} );
        var target;
        it('instantiating', function () {
            expect( target = new DataBinder( source ) ).toBeDefined();
        });
        describe('get', function () {
            it('source[ < key > ]',function () {
                expect(source['a']).toEqual(24);
            });
            it('target[ < key > ]',function () {
                expect(target['a']).toEqual(24);
            });
        });
        describe('set',function () {
            it('source[< key >] = < value >', function () {
                source['a'] = 13;
                expect( source['a'] ).toEqual(13);
            });
            it('changes in source[< key >] should not propagate to target[ < key > ]', function () {
                expect( target['a'] ).toEqual(24);
            });
            it('target[< key >] = < value >', function () {
                target['a'] = 14;
                expect( target['a'] ).toEqual(14);
            });
            it('changes in target[< key >] should not propagate to source[ < key > ]', function () {
                expect( source['a'] ).toEqual(13);
            });
        });
    });

    describe('use factory method', function () {
        var model = DataBinder.createRecursiveBinder( { user:{id:24} } );
        it('model[ < key[0] > ] should be instance of DataBinder', function () {
            expect( DataBinder.isBinder( model['user'] ) ).toBe(true);
        });
        describe('get',function () {
            it('model[ < key[0] > ][ < key[1] >]...[ < key[n] > ] ',function () {
                expect( model['user']['id'] ).toEqual( 24 );
            });
        });
        //TODO add test to verify if child node of type DataBinder to another value it should bind new value and remove old observer on 'update'
    });
});
